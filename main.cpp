#include <iostream>

using namespace std;

int main() 
{
	cout<<"please input number :";
	int number=0;
	cin>>number;
	cout<<"this is square of the number you have given :"<< number*number <<endl;
	cout<<"this is cube of the number you have given :" << number*number*number << endl;
	cout<<"this is twice the value of the given number : " << number * 2 << endl;
	cout<<"this is thrice the value of the given number : " << number * 3 << endl;
	return 0;
}
